<?php 

// Ajouter la prise en charge des images mises en avant
add_theme_support( 'post-thumbnails' );

// Ajouter automatiquement le titre du site dans l'en-tête du site
add_theme_support( 'title-tag' );

// Désactive la barre d'outils d'administration
add_filter('show_admin_bar', '__return_false');

if(!function_exists('deep_purple_cpt')) {

    function deep_purple_cpt() {

    // -----MEMBRES-----MEMBRES-----MEMBRES-----MEMBRES-----MEMBRES-----MEMBRES-----MEMBRES-----MEMBRES-----MEMBRES-----MEMBRES
        $labels_membre = [
            "name"                  => __( "Membres", "enssop" ),
            "singular_name"         => __( "Membre", "enssop" ),
            "menu_name"             => __( "Membres", "enssop" ),
            "all_items"             => __( "Tous les membres", "enssop" ),
            "add_new"               => __( "Ajouter", "enssop" ),
            "add_new_item"          => __( "Ajouter un nouveau membre", "enssop" ),
            "new_item"              => __( "Nouveau membre", "enssop" ),
            "view_item"             => __( "Voir le membre", "enssop" ),
            "view_items"            => __( "Voir les membres", "enssop" ),
            "search_items"          => __( "Chercher un membre", "enssop" ),
            "not_found"             => __( "Pas de vếtements trouvé", "enssop" ),
            "not_found_in_trash"    => __( "Pas de membres trouvé dans la corbeille", "enssop" ),
            "featured_image"        => __( "Image mise en avant pour ce membre", "enssop" ),
            "set_featured_image"    => __( "Définir l'image mise en avant pour ce membre", "enssop" ),
            "remove_featured_image" => __( "Supprimer l'image mise en avant pour ce membre", "enssop" ),
            "use_featured_image"    => __( "Utiliser comme image mise en avant pour ce membre", "enssop" ),
            "archives"              => __( "Type de membres", "enssop" ),
            "insert_into_item"      => __( "Ajouter à membres", "enssop" ),
            "uploaded_to_this_item" => __( "Ajouter à membres", "enssop" ),
            "filter_items_list"     => __( "Filtrer la liste des membres", "enssop" ),
            "items_list_navigation" => __( "Naviguer dans la liste des membres", "enssop" ),
            "items_list"            => __( "Liste des membres", "enssop" ),
            "attributes"            => __( "Paramètres du membre", "enssop" ),
            "name_admin_bar"        => __( "membres", "enssop" ),
        ];

        $args_membre = [
            "label"                 => __('Membres', 'enssop'),
            "labels"                => $labels_membre,
            "description"           => __('Membres du site', 'enssop'),
            "public"                => true,
            "publicly_queryable"    => true,
            "show_ui"               => true,
            "delete_with_user"      => false,
            "show_in_rest"          => false,
            "has_archive"           => true,
            "show_in_menu"          => true,
            "show_in_nav_menu"      => true,
            "menu_position"         => 8,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array( "slug" => "membres", "with_front" => true ),
            "query_var"             => 'membres',
            "menu_icon"             => "dashicons-businessperson",
            "supports"              => array( "title", 'editor', 'thumbnail' ),
        ];

        register_post_type( 'membre', $args_membre );



        // -----IMAGES-----IMAGES-----IMAGES-----IMAGES-----IMAGES-----IMAGES-----IMAGES-----IMAGES-----IMAGES-----IMAGES-----IMAGES
        $labels_image = [
            "name"                  => __( "Images", "enssop" ),
            "singular_name"         => __( "Image", "enssop" ),
            "menu_name"             => __( "Images", "enssop" ),
            "all_items"             => __( "Toutes les images", "enssop" ),
            "add_new"               => __( "Ajouter", "enssop" ),
            "add_new_item"          => __( "Ajouter une nouvelle image", "enssop" ),
            "new_item"              => __( "Nouvelle image", "enssop" ),
            "view_item"             => __( "Voir l'image", "enssop" ),
            "view_items"            => __( "Voir les images", "enssop" ),
            "search_items"          => __( "Chercher une image", "enssop" ),
            "not_found"             => __( "Pas d'image trouvé", "enssop" ),
            "not_found_in_trash"    => __( "Pas d'images trouvé dans la corbeille", "enssop" ),
            "featured_image"        => __( "Image mise en avant pour cette image", "enssop" ),
            "set_featured_image"    => __( "Définir l'image mise en avant pour cette image", "enssop" ),
            "remove_featured_image" => __( "Supprimer l'image mise en avant pour cette image", "enssop" ),
            "use_featured_image"    => __( "Utiliser comme image mise en avant pour cette image", "enssop" ),
            "archives"              => __( "Type d'images", "enssop" ),
            "insert_into_item"      => __( "Ajouter à images", "enssop" ),
            "uploaded_to_this_item" => __( "Ajouter à images", "enssop" ),
            "filter_items_list"     => __( "Filtrer la liste des images", "enssop" ),
            "items_list_navigation" => __( "Naviguer dans la liste des images", "enssop" ),
            "items_list"            => __( "Liste des images", "enssop" ),
            "attributes"            => __( "Paramètres du image", "enssop" ),
            "name_admin_bar"        => __( "images", "enssop" ),
        ];

        $args_image = [
            "label"                 => __('Images', 'enssop'),
            "labels"                => $labels_image,
            "description"           => __('Images du site', 'enssop'),
            "public"                => true,
            "publicly_queryable"    => true,
            "show_ui"               => true,
            "delete_with_user"      => false,
            "show_in_rest"          => false,
            "has_archive"           => true,
            "show_in_menu"          => true,
            "show_in_nav_menu"      => true,
            "menu_position"         => 8,
            "exclude_from_search"   => false,
            "capability_type"       => "post",
            "map_meta_cap"          => true,
            "hierarchical"          => false,
            "rewrite"               => array( "slug" => "gallerie", "with_front" => true ),
            "query_var"             => 'gallerie',
            "menu_icon"             => "dashicons-media-default",
            "supports"              => array( "title", 'editor', 'thumbnail' ),
        ];

        register_post_type( 'gallerie', $args_image );

    }
}

add_action( 'init', 'deep_purple_cpt');


// AJOUTER DES FEUILLES DE STYLE
if (!function_exists('addStylesheets')) {
    function addStylesheets() {
        //wp_enqueue_style("my-stylesheet", get_stylesheet_directory_uri().'/style.css');
        // paramètre - $handle: nom de la feuille de style, $src: source des liens
        wp_enqueue_style("bootstrap","https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css");
        // ajout du css bootstrap
        wp_enqueue_script("jquery","https://code.jquery.com/jquery-3.4.1.slim.min.js");
        wp_enqueue_script("jquery","https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js");
        wp_enqueue_script("bootstrap-stackpath","https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js");
        // ajout des scripts bootstrap
    }
}

add_action("wp_enqueue_scripts", "addStylesheets");


register_nav_menus([
    "nav_menu" => __('Menu principal','enssop'),
    "footer_menu" => __('Menu de pied de page','enssop')
]);

// AJOUTER UNE SIDEBAR POUR WIDGETS
if( function_exists('register_sidebar')){

    register_sidebar([
        'name' => 'sidebar',
        'id' => 'sidebar-1',
        'before_widget' => '<div class="widget-sidebar">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ]);
}
