
<?php get_header(); 

        if ( have_posts() ) {
            while ( have_posts() ) {
                the_post(); ?>

                    <div class="container mt-4">
                        <div class="jumbotron mb-4">
                            <h1 class="display-4 text-center"><?php the_title() ?></h1>
                        </div>
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <img src="<?php the_post_thumbnail_url() ?>" class="w-100" alt="">
                            </div>
                            <div class="col-md-6">
                                <?php the_content() ?>
                            </div>
                        </div>
                    </div>

                    <div>date d'entrée dans le groupe: <?php the_field("date_dentree") ?></div>
                    <div>
                        <img src="<?php the_field("photo") ?>" alt="">
                    </div>
<?php       }
        }
get_footer(); ?>
