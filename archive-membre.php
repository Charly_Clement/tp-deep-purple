
<?php get_header(); ?>

    <div class="container mt-4">
        <div class="jumbotron mb-4">
            <h1 class="display-4 text-center">Membres</h1>
        </div>
    </div>
<div class="container">
           <div class="row">
<?php 
    if ( have_posts() ) {
        while ( have_posts() ) {
            the_post();
?>
        
                <div class="col-4 mb-4">
                    <div class="card">
                        <img src="<?php the_post_thumbnail_url() ?>" class="w-100" alt="">
                        <div class="card-body">
                            <p class="card-text"><?php the_title(); ?></p>
                        </div>
                        <div class="card-body">
                            <a href="<?php the_permalink() ?>" class="card-link text-success border border-success p-2 rounded">View</a>
                        </div>
                    </div>
                </div>
          
<?php   }
    } ?></div>
        </div>
  <?php  get_footer();

?>
  