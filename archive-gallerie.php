
<?php get_header(); ?> 

    <div class="container mt-4">
        <div class="jumbotron mb-4">
            <h1 class="display-4 text-center">Gallerie</h1>
        </div>
    </div>

    <div class="container">
        <div class="row">

<?php 
        if ( have_posts() ) {
            while ( have_posts() ) {
                the_post(); ?>
                    <div class="col-4 mb-4">
                        <div class="card" style="width:300px; height:300px">
                            <img src="<?php the_post_thumbnail_url() ?>" class="w-100" alt="">
                        </div>
                    </div>
<?php   }
            } ?>
        </div>
    </div>

<?php get_header(); ?>