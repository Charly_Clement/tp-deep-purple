<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TP Deep Purple</title>
    <?php wp_head(); ?>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light text-white bg-light">
        <a class="navbar-brand" href="#">Deep Purple</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav text-white">
                <!-- <a class="nav-item nav-link text-white active" href="#">Membres actifs <span class="sr-only">(current)</span></a>
                <a class="nav-item nav-link text-white" href="#">Ancien membres</a>
                <a class="nav-item nav-link text-white" href="#">Gallerie photo</a> -->
                <?php
                    wp_nav_menu([
                        "theme_location" => 'nav_menu',
                        "menu_class" => 'navbar-nav',
                    ])
                ?>
            </div>
        </div>
    </nav>
